#!/usr/bin/env python3
"""
http://www.sviluppoeconomico.gov.it/images/exportCSV/anagrafica_impianti_attivi.csv
https://www.sviluppoeconomico.gov.it/images/exportCSV/prezzo_alle_8.csv
"""
import json
#import requests

from geojson import Point, Polygon, FeatureCollection, Feature, dumps
import pandas as pd
import numpy as np



#for fname in ('anagrafica_impianti_attivi.csv','prezzo_alle_8.csv'):
#    with open(fname,'wb') as f:
#        r = requests.get('http://www.sviluppoeconomico.gov.it/images/exportCSV/'+fname)
#        f.write(r.content)


b_reg = pd.read_csv("anagrafica_impianti_attivi.csv", encoding="latin-1", sep=";",header=1, on_bad_lines='warn')
impianti = pd.read_csv("prezzo_alle_8.csv", encoding="latin-1", sep=";",header=1, on_bad_lines='warn')
mb = pd.merge(b_reg,impianti,on='idImpianto')


metano = mb[(mb.descCarburante == 'Metano')]

# Fix missing values
metano = metano.fillna(0)

minlat = np.floor(metano.Latitudine.min())
maxlat = np.ceil(metano.Latitudine.max())

minlon = np.floor(metano.Longitudine.min())
maxlon = np.ceil(metano.Longitudine.max())


for lat in np.arange(minlat, maxlat,0.1):
    for lng in np.arange(minlon, maxlon,0.1):

        swlng,swlat,nelng,nelat = round(lng - 0.5,5) , round(lat -0.5,5), round(lng + 0.5,5), round(lat + 0.5,5)
        #print(swlng)

        features = []
        r = metano[ (metano.Latitudine > swlat) & 
                    (metano.Latitudine < nelat) &
                    (metano.Longitudine > swlng) &
                    (metano.Longitudine < nelng)
                  ]
        
        for index, row in r.iterrows():

            p = Point((row.Longitudine, row.Latitudine))
            features.append(Feature(geometry=p, properties={'gestore':row.Gestore,
                                                            'bandiera':row.Bandiera,
                                                            'prezzo': "{:.3f}".format(row.prezzo),
                                                            'carburante': row.descCarburante,
                                                            'autostrada': row["Tipo Impianto"]=='Autostradale' }))
        if features:
            features.append(Polygon([[(swlng,swlat), (swlng, nelat), (nelng, nelat), (nelng, swlat), (swlng,swlat)]])
                            )
            fc = FeatureCollection(features)
            try:
                with open('public/jsons/metano_{:.1f}_{:.1f}.json'.format(lat,lng),'w') as f:
                    f.write(dumps(fc, sort_keys=True))
            except:
                print('Failed with one of these ', r.idImpianto)

